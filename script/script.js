let newUser = {};
function createNewUser(name , lastName) {
    name = prompt('Enter your name');
    lastName = prompt('Enter your last name');
    birthday = prompt('Enter your birth date: dd.mm.yyyy', 'dd.mm.yyyy');
    let userDate = birthday.slice(0, 2) + birthday.slice(3, 5) + birthday.slice(6);
    let today = new Date();
    let userBirthday = new Date(userDate.slice(2,4) + '-' + userDate.slice(0,2) + '-' + userDate.slice(4));
    while (userBirthday > today || birthday.length < 10 || isNaN(+userDate)) {
        birthday = prompt('Enter your birth date: dd.mm.yyyy', 'dd.mm.yyyy');
    }
    newUser = {
        firstName : name,
        lastName : lastName,
        birthday : userBirthday,
        getLogin() {
        return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        getAge(){
        return Math.floor((new Date() - Date.parse(this.birthday))/1000/60/60/24/365.25);
        },
        getPassword(){
        // let date = new Date(birthday);
        return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + userBirthday.getFullYear();
        }
    }
    return newUser;
}
createNewUser();
console.log(newUser);
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());

